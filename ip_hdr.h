#ifndef IP_HDR_H
#define IP_HDR_H

/* IP header */
///
/// \brief The sniff_ip struct defines the packet header of a
/// tcp internet packet. Used to extract IP layer information from a packet.
struct sniff_ip {
        u_char  ip_vhl;                 /**< version << 4 | header length >> 2 */
        u_char  ip_tos;                 /**< type of service / tipo de servicio*/
        u_short ip_len;                 /**< total length / largo total*/
        u_short ip_id;                  /**< identification / identificacion */
        u_short ip_off;                 /**< fragment offset field / offset del campo de fragmento*/
        #define IP_RF 0x8000            /**< reserved fragment flag / bandera de fragmento reservada*/
        #define IP_DF 0x4000            /**< dont fragment flag / bander de no fragmentar */
        #define IP_MF 0x2000            /**< more fragments flag / mas banderas de framentos*/
        #define IP_OFFMASK 0x1fff       /**< mask for fragmenting bits / mascara para bits de fragmentacion */
        u_char  ip_ttl;                 /**< time to live / tiempo de vida*/
        u_char  ip_p;                   /**< protocol / protocolo  */
        u_short ip_sum;                 /**< checksum */
        struct  in_addr ip_src,ip_dst;  /**< source and dest address / direccion fuente y destino */
};
#define IP_HL(ip)               (((ip)->ip_vhl) & 0x0f)
#define IP_V(ip)                (((ip)->ip_vhl) >> 4)

#endif // IP_HDR_H
